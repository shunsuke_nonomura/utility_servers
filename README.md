# utility_servers
webサーバー構築に便利なサーバーを集めたもの。
docker-compose 利用前提。
パラメータは適宜変更する。

参考リンク
https://qiita.com/okamu_/items/f6875d5bce2ac6752758

## init
初期化手段まとめ

## database
- postgresql, pgadmin4
- mongodb, mongo express
  - https://qiita.com/mistolteen/items/ce38db7981cc2fe7821a
- elastic search + kibana + logstash
  - https://catalina1344.hatenablog.jp/entry/2019/01/20/170821

## scheduler
- python_scheduler

## monitoring
- portainer
  - https://qiita.com/ao_log/items/d8ef847c826746f9e84b

## knowledge_base
- knowledge
