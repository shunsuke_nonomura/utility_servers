#coding:UTF-8
import schedule
import time
import requests
import datetime
import json
from aescipher import pass_phrase, AESCipher

def job():
    #ここにメインの処理を書く
   print(0)

#jobを定期実行
schedule.every(60).minutes.do(job)

# 初回実行（テスト目的）
job()
while True:
    schedule.run_pending()
    time.sleep(1)
