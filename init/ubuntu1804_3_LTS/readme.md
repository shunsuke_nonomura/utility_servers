# 概要
ubuntu18.04.3 LST用セットアップ。
シェルを読み込んで、必要に応じて適宜実行する。
- Ubuntu 18.04 LTS 日本語 Remix リソース(18.04.3)
  - https://www.ubuntulinux.jp/News/ubuntu1804-ja-remix

## ファイル
- samba　:　samba設定用
- xrdp　:　リモートデスクトップ(xorgバグ対応版)
- gpu　:　gpu利用する場合
- docker　:　docker設定
- nvidia_docker　:　docker上でgpu利用する場合
- check_nvidia_docker : nvidia-dockerの確認用リソース
- 02-allow-colord.conf : xrdpのカラーマネジメント対応リソース

# コマンドメモ
## ユーザ追加
```
sudo adduser [username] 
```

## スーパーユーザ設定
```
sudo gpasswd -a [username] sudo 
```

## docker実行権限追加(dockerグループがない場合は作成から)
```
sudo groupadd docker
sudo usermod -aG docker [username] 
```

## firewallのポート許可
```
sudo ufw allow 3389 
sudo ufw reload 
```

# その他対応メモ
## システムの問題が見つかりましたがログイン時に出る
```
sudo rm /var/crash/*
sudo sed -i 's/enabled=1/enabled=0/g' /etc/default/apport
```
https://qiita.com/naoyukisugi/items/d0a30f1e7b9761fdf3e9

# 情報
## XRDP関連で詰まりやすい部分
- Ubuntus18.04.2 LTE 以降でxorg関連で公式にバグがあり、xorgxrdpのインストールを工夫する必要がある
  - dpkg -l|grep xorgxrdp　でデフォルトで何も表示されない 
  - sudo apt install xorgxrdp　単体でインストールできない 
- リモートデスクトップログイン時にカラーマネジメントデバイスで認証が必要という表示が出る

Ubuntus18.04.02でｘorg関連インストールでバグがある 
https://note.spage.jp/archives/576 

18.04.3 LTS XRDP “login failed for display: 0”
https://askubuntu.com/questions/1164456/18-04-3-lts-xrdp-login-failed-for-display-0 

カラーマネジメントデバイスの認証について 
https://www.server-memo.net/memo/xrdp.html 


## docker関連で詰まりやすい部分
- リポジトリ経由でなければdocker ceがインストールできない

docker install
https://docs.docker.com/install/linux/docker-ce/debian/ 

docker-compose のバージョンリスト
https://github.com/docker/compose/releases

nvidia-dockerを使用してGPU環境構築
https://qiita.com/gen10nal/items/1e7fe8a1b2e9ad1e7919

## samba関連参考ページ
http://engetu21.hatenablog.com/entry/2018/04/01/222637 

https://lab4ict.com/system/archives/1175 

